{
  "_id": "1i8b1v3dngkxj6jp",
  "name": "Magic Circle against Evil",
  "type": "spell",
  "img": "systems/pf1/icons/misc/magic-swirl.png",
  "effects": [],
  "folder": null,
  "sort": 0,
  "flags": {},
  "system": {
    "tags": [],
    "actions": [
      {
        "_id": "5dipupgrilsei9i4",
        "name": "Use",
        "img": "systems/pf1/icons/misc/magic-swirl.png",
        "description": "",
        "tag": "",
        "activation": {
          "cost": 1,
          "type": "standard"
        },
        "unchainedAction": {
          "activation": {
            "cost": 2,
            "type": "action"
          }
        },
        "duration": {
          "value": "10 min./level",
          "units": ""
        },
        "target": {
          "value": ""
        },
        "range": {
          "value": null,
          "units": "touch",
          "maxIncrements": 1,
          "minValue": null,
          "minUnits": ""
        },
        "uses": {
          "autoDeductCharges": true,
          "autoDeductChargesCost": "1",
          "self": {
            "value": 0,
            "maxFormula": "",
            "per": null
          },
          "per": "",
          "value": 0,
          "maxFormula": ""
        },
        "measureTemplate": {
          "type": "circle",
          "size": "10",
          "overrideColor": false,
          "customColor": "",
          "overrideTexture": false,
          "customTexture": ""
        },
        "attackName": "",
        "actionType": "spellsave",
        "attackBonus": "",
        "critConfirmBonus": "",
        "damage": {
          "parts": [],
          "critParts": [],
          "nonCritParts": []
        },
        "attackParts": [],
        "formulaicAttacks": {
          "count": {
            "formula": ""
          },
          "bonus": {
            "formula": ""
          },
          "label": null
        },
        "formula": "",
        "ability": {
          "attack": "",
          "damage": "",
          "damageMult": 1,
          "critRange": 20,
          "critMult": 2
        },
        "save": {
          "dc": "0",
          "type": "will",
          "description": "Will negates (harmless)"
        },
        "effectNotes": [],
        "attackNotes": [],
        "soundEffect": "",
        "powerAttack": {
          "multiplier": "",
          "damageBonus": 2,
          "critMultiplier": 1
        },
        "naturalAttack": {
          "primaryAttack": true,
          "secondary": {
            "attackBonus": "-5",
            "damageMult": 0.5
          }
        },
        "nonlethal": false,
        "usesAmmo": false,
        "spellEffect": "",
        "spellArea": "10-ft.-radius emanation from touched creature",
        "conditionals": [],
        "enh": {
          "value": null
        }
      }
    ],
    "uses": {
      "per": ""
    },
    "attackNotes": [],
    "effectNotes": [],
    "links": {
      "children": []
    },
    "flags": {
      "boolean": [],
      "dictionary": []
    },
    "scriptCalls": [],
    "learnedAt": {
      "class": [
        ["sorcerer/wizard", 3],
        ["cleric/oracle", 3],
        ["paladin", 3],
        ["summoner", 3],
        ["inquisitor", 3],
        ["shaman", 3],
        ["medium", 3],
        ["occultist", 3],
        ["spiritualist", 3],
        ["unchained Summoner", 3]
      ],
      "domain": [["Good", 3]],
      "subDomain": [],
      "elementalSchool": [],
      "bloodline": [["Celestial", 3]]
    },
    "level": 3,
    "school": "abj",
    "types": "good",
    "components": {
      "verbal": true,
      "somatic": true,
      "material": true,
      "divineFocus": 2
    },
    "materials": {
      "value": "a 3-ft.-diameter circle of powdered silver"
    },
    "sr": false,
    "shortDescription": "<p>All creatures within the area gain the effects of a <i>Protection from Evil</i> spell, and evil summoned creatures cannot enter the area either. Creatures in the area, or who later enter the area, receive only one attempt to suppress effects that are controlling them.</p><p>If successful, such effects are suppressed as long as they remain in the area. Creatures that leave the area and come back are not protected. You must overcome a creature's spell resistance in order to keep it at bay (as in the third function of <i>Protection from Evil</i>), but the deflection and resistance bonuses and the protection from mental control apply regardless of enemies' spell resistance.</p><p>This spell has an alternative version that you may choose when casting it. A <i><i>magic circle</i> against evil</i> can be focused inward rather than outward. When focused inward, the spell binds a nongood called creature (such as those called by the <i><i>lesser planar binding</i>, planar binding,</i> and <i>greater planar binding</i> spells) for a maximum of 24 hours per caster level, provided that you cast the spell that calls the creature within 1 round of casting the <i>magic circle</i>. The creature cannot cross the circle's boundaries. If a creature too large to fit into the spell's area is the subject of the spell, the spell acts as a normal <i>Protection from Evil</i> spell for that creature only.</p><p>A <i>magic circle</i> leaves much to be desired as a trap. If the circle of powdered silver laid down in the process of spellcasting is broken, the effect immediately ends. The trapped creature can do nothing that disturbs the circle, directly or indirectly, but other creatures can. If the called creature has spell resistance, it can test the trap once a day. If you fail to overcome its spell resistance, the creature breaks free, destroying the circle.</p><p>A creature capable of any form of dimensional travel (astral projection, blink, dimension door, etherealness, gate, plane shift, shadow walk, teleport, and similar abilities) can simply leave the circle through such means. You can prevent the creature's extradimensional escape by casting a <i>dimensional <i>anchor</i></i> spell on it, but you must cast the spell before the creature acts. If you are successful, the <i>anchor</i> effect lasts as long as the <i>magic circle</i> does. The creature cannot reach across the <i>magic circle</i>, but its ranged attacks (ranged weapons, spells, magical abilities, and the like) can. The creature can attack any target it can reach with its ranged attacks except for the circle itself.</p><p>You can add a special diagram (a two-dimensional bounded figure with no gaps along its circumference, augmented with various magical sigils) to make the <i>magic circle</i> more secure. Drawing the diagram by hand takes 10 minutes and requires a DC 20 Spellcraft check. You do not know the result of this check. If the check fails, the diagram is ineffective. You can take 10 when drawing the diagram if you are under no particular time pressure to complete the task.</p><p>This task also takes 10 full minutes. If time is no factor at all, and you devote 3 hours and 20 minutes to the task, you can take 20.</p><p>A successful diagram allows you to cast a <i>dimensional <i>anchor</i></i> spell on the <i>magic circle</i> during the round before casting any summoning spell. The <i>anchor</i> holds any called creatures in the <i>magic circle</i> for 24 hours per caster level. A creature cannot use its spell resistance against a <i>magic circle</i> prepared with a diagram, and none of its abilities or attacks can cross the diagram. If the creature tries a Charisma check to break free of the trap (see the <i>lesser planar binding</i> spell), the DC increases by 5. The creature is immediately released if anything disturbs the diagram-even a straw laid across it. The creature itself cannot disturb the diagram either directly or indirectly, as noted above.</p><p>This spell is not cumulative with <i>Protection from Evil</i> and vice versa.</p>"
  },
  "ownership": {
    "default": 0
  },
  "_stats": {
    "systemId": "pf1",
    "systemVersion": "0.82.1",
    "coreVersion": "10.285",
    "createdTime": 1663018730165,
    "modifiedTime": 1663018744589,
    "lastModifiedBy": "DbeAbHwpEqczKjsD"
  }
}
